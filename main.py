from autocompleteprovider import AutoCompleteProvider


def print_menu():
    print("1: Train auto-complete")
    print("2: Output auto-complete suggestions")
    print("3: EXIT")


def print_words(words):
    length = len(words)
    for x in range(length):
        word = words[x]
        if x is not length - 1:
            # output "word" (confidence)
            print('"{}"({}) '.format(word.get_word(), word.get_confidence()), end=",")
        else:
            print('"{}"({}) '.format(word.get_word(), word.get_confidence()), end="\n\n")


if __name__ == "__main__":
    auto_complete = AutoCompleteProvider()
    loop = True
    while loop:
        print_menu()
        menu_choice = int(input("\nPlease choose an option:  "))
        if menu_choice is 1:
            user_in = input("\nPlease input a training passage:  ")
            auto_complete.train(user_in)
        elif menu_choice is 2:
            user_in = input("\nPlease input a word fragment:  ")
            words = auto_complete.get_words(user_in)
            print_words(words)
        elif menu_choice is 3:
            print("Goodbye")
            loop = False
            break
