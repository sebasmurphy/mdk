from candidate import Candidate
import re


class AutoCompleteProvider:
    """An autocomplete provider

    AutoCompleteProvider is a dictionary whcih contains a trie that facilitates inserting values and returning
    a list<candidate> of values that share a common prefix

    Attributes:
       self.words (dict<string>): a trie containing a list of strings that have been entered into the system
    """

    def __init__(self):
        """init function"""
        self.words = {}

    def __sanitize(self, text):
        """Sanitizes a passage of text

        Checks the text to see if it is in fact a string
        Removes unnecessary punctuation.
        Strips beginning and ending whitespace
        Converts the text to lowercase

        Args:
            text (str) : text to be sanitized

        Returns:
            text(str): sanitized text

        Raises:
            ValueError: if the passage is not a string
        """
        if not isinstance(text, str):
            raise ValueError("text passage is not a string")
        # strip out unnecessary characters
        text = text.strip()
        # text = text.translate(" ?.!/;:")
        text = re.sub(r'[^a-zA-Z0-9]', '', text)
        text = text.lower()
        return text

    def train(self, passage):
        """Inserts a passage of text

        The passage of text will be sanitized before being inserted

        Args:
            passage (str) : pass of text to be inserted

        Returns:
            bool: True if the insert was successful. None otherwise
        """
        if len(passage) > 0:
            # strip out unnecessary characters
            passage = passage.split()
            for x in passage:
                try:
                    x = self.__sanitize(x)
                except ValueError:
                    return None
                self.__train(x)
        return True

    def __train(self, value, dict=None):
        """Inserts a value into the list of words

        Args:
            value (str) : word to be inserted
            dict (dictionary) : dictionary in which value is to be inserted
        Returns:
            bool: True if the insert was successful. None otherwise

        Raises:

        """
        if not dict:
            dict = self.words
        head = value[0]

        if head in dict:
            node = dict[head]
        else:
            node = {"value": head, "confidence": 0}
            dict[head] = node
        if len(value) > 1:
            # value has characters left to add
            # recurse
            return self.__train(value[1:], node)
        else:
            node["value"] = value
            # increase confidence of the node
            node["confidence"] += 1
            return True

    def contains(self, value):
        """Searches for a given string value a dictionary.

           Args:
               value (str): Value to search
               dict(dictionary<list>)(optional): dictionary in which to search
           Returns:
               bool: True if the item was found. None otherwise

           Raises:
               ValueError: if value is not a string
           """

        try:
            value = self.__sanitize(value)
        except ValueError:
            return None
        return self.__contains(value, self.words)

    def __contains(self, value, dict):
        # Private function

        head = value[0]
        if head not in dict:
            return None

        if len(value) > 1:
            node = dict[head]
            return self.__contains(value[1:], node)
        else:
            return True

    def get_words(self, fragment):
        """Retrieves a list of candidates from the dictionary

        Candidates all have fragment as a common prefix

        fragment will be sanitized before the search begins

        Args:
            fragment (str): word fragment to search for

        Returns:
            list<Candidate>: A sorted list containing candidate values sorted by confidence

            ex. [["word":'this',"confidence": 2], ["word":'that',"confidence": 1], ["word":'temp',"confidence": 1]]

            If no values in the string match the prefix and empty list will be returned

            []

        Raises:
            ValueError: if the fragment is not a string.
        """
        to_return = []

        try:
            fragment = self.__sanitize(fragment)
        except ValueError:
            return to_return

        parent_str = ""
        head = fragment[0]
        # if head in self.children:
        if head in self.words:
            node = self.words[head]
            self.__get_words(to_return, parent_str, fragment[1:], node)
        to_return.sort(key=lambda x: (x.confidence, x.word), reverse=True)
        return to_return

    def __get_words(self, to_return, parent_str, fragment, dict):
        # Private function
        #
        # string containing the values from parent nodes - used to recreate candidate word
        parent_str += dict["value"]
        if dict["confidence"] > 0:
            to_return.append(Candidate(parent_str, dict["confidence"]))
        if len(fragment) >= 1:
            head = fragment[0]
            node = dict[head]
            self.__get_words(to_return, parent_str, fragment[1:], node)
        else:
            for x in dict:
                # "value" and "confidence" are keys int dict but do not represent child nodes
                if x is not "value" and x is not "confidence":
                    node = dict[x]
                    self.__get_words(to_return, parent_str, fragment[1:], node)
