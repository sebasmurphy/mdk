import unittest
from autocompleteprovider import AutoCompleteProvider
from candidate import Candidate

class AutoCompleteTest(unittest.TestCase):

    def setUp(self):
        self.dict = AutoCompleteProvider()

    def test_basic(self):
        self.assertEqual(self.dict.words, {})

    def test_basic_insert(self):
        self.assertTrue(self.dict.train("th"))
        self.assertTrue(self.dict.train("th"))
        self.assertTrue("t" in self.dict.words)
        t_child = self.dict.words["t"]
        self.assertEqual(t_child["confidence"], 0)
        self.assertEqual(t_child["value"], "t")
        self.dict.train("apple")
        self.assertTrue("a" in self.dict.words)
        self.dict.train("a")
        t_child = self.dict.words["a"]
        self.assertEquals(t_child["confidence"], 1)
        self.assertEquals(t_child["value"], "a")

    def test_basic_find(self):
        self.dict.train("th")
        x = self.dict.contains("th")
        self.assertTrue(self.dict.contains("th"))
        self.dict.train("this")
        self.assertTrue(self.dict.contains("this"))
        self.assertFalse(self.dict.contains("apple"))

    def test_basic_confidence(self):
        self.dict.train("this")
        self.dict.train("this")
        self.dict.train("that")
        x = Candidate("this",2)
        y = Candidate("that",1)
        result = self.dict.get_words("t")
        self.assertEqual(len(result),2)
        self.assertEqual(x.get_word(), result[0].get_word())
        self.assertEqual(y.get_word(), result[1].get_word())


if __name__ == '__main__':
    unittest.main()