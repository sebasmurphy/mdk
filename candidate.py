class Candidate(object):
    """An autocomplete candidate

    A candidate represents a word. A candidate maintains it's value and confidence.
    Confidence represents a ranking so that it may be compared to other candidate values.

    Attributes:
        word(str): candidate word
        confidence(int): candidate confidence value
    """

    def __init__(self, word, confidence):
        """init function

        Args:
            word(str)
            confidence(int)
        """
        self.word = word
        self.confidence = confidence

    def get_word(self):
        """Get candidate word

        Returns:
            word(str)
       """
        return self.word

    def get_confidence(self):
        """Gets candidate confidence


          Returns:
              confidence(int)
          """
        return self.confidence

    def equals(self, other):
        """Determines whether the candidate that invokes this method is equal to the candidate passed as argument

          Args:
              other(candidate)

          Returns:
              True if candidates are equal. Otherwise False
          """
        to_return = False
        to_return = self.confidence is other.confidence
        to_return = to_return and self.word is other.word
        return to_return

    def compare(self, other):
        """Determines whether the candidate that invokes is greater than, less than, or equal to the candidate passed
        as argument

          Args:
              other(candidate)

          Returns:
              Positive integer if candidate is greater than the argument, Negative integer if candidate is less than
              the argument, 0 if the candidate is equal to the argument
          """
        return self.confidence - other.confidence