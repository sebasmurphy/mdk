import main
from autocompleteprovider import AutoCompleteProvider
import unittest
import sys
from io import StringIO



class MainTest(unittest.TestCase):
    def setUp(self):
        self.dict = AutoCompleteProvider()
        self.passage = "The Thing That"
        self.fragment = "th"
        self.output = '"thing"(1) ,"the"(1) ,"that"(1) \n\n'

    def test_basic(self):
        self.dict.train(self.passage)
        sys.stdout = StringIO()  # capture output
        main.print_words(self.dict.get_words(self.fragment))
        out = sys.stdout.getvalue()  # release output
        self.assertEqual(self.output, out)

if __name__ == '__main__':
    unittest.main()
