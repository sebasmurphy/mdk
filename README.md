## MDK - Mobile Device Keyboard

MDK is a python command line app that simulates a mobile device keyboard autocomplete system. MDK provides a listing of auto-complete candidates based on user input, similar to how the keyboard on apple iphone or android provides suggestions as text is typed. As more text is entered into MDK it's accuracy will improve. MDK ranks suggestions based on confidence. In MDK, confidence is a function of the number of times a word has been seen by the system

The System
  - Allows new text to be inserted to increase accuracy of suggestions
  - Outputs a list of autocomplete candidate suggestions based on current keyboard input
  - Supports words that contains numbers ex. l33t speak

## Design

MDK was designed to optimize for space and time. I felt that retrevial speed of candidates was of ciritcal importance and designed the system with that in mind

I used dictionaries to create a trie that stores the string values added to MDK. I felt that this would offer the best trade-off between time and space. Dictionaries are quick to do lookups(amortiezed O(1)) and use far less memory/space than objects.

Each dictionary stores it's 'value':(string), 'confidence':(dict). Children nodes are stored as 'key'(character): 'value'(dict)

Below is an example of a trie containing the word 'At'

```python
trie = {"value:","confidence":0,"a":{"value":"a","confidence":0, "t":{"value":"t","confidence":0}}}
```

### Version
1.0

### Tech

* [Python] - A programming language that lets you work quickly and integrate systems more effectively

### Installation

Mobile Device Keyboard requires [Python] v3.5+ to run

**OSX**
requires [HomeBrew] to be installed
```sh
$ brew install python3
```
**Other Platforms**
Use the directions listed at <https://www.python.org/downloads/>

```sh
$ git clone https://gitlab.com/sebasmurphy/mdk.git
```

### Usage

```sh
$ cd mdk
$ python3 main.py
```

The command line app has three options. After selecting an option the console will wait for user input before outputting the results

>The auto-complete must be trained before suggestions can be output

`1:Train auto-complete`

Input:  Text passage to train the autocomplete  
Output:  None


`2:Output auto-complete suggestions`

Input:  Text fragment to retrieve suggestions  
Output:  List of auto-complete candidates ordered by confidence
 
`3:Exit`  
Exits the Program

### Todos

 - Add Delete functionality
 - Convert from trie to ternary search tree - Reduce storage space
 - Add additional error checking
 - Rethink input sanitization

License
----

MIT


**Free Software, Hell Yeah!**

[//]: # (These are reference links used in the body of this note and get stripped out when the markdown processor does its job. There is no need to format nicely because it shouldn't be seen. Thanks SO - http://stackoverflow.com/questions/4823468/store-comments-in-markdown-syntax)



   [Python]: <https://www.python.org/>
   [PythonDownloads]: <https://www.python.org/downloads/>
   [HomeBrew]: <http://brew.sh>
   [git-repo-url]: <https://gitlab.com/sebasmurphy/mdk.git>
