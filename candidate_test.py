import unittest
from candidate import Candidate


class CandidateTest(unittest.TestCase):
    def setUp(self):
        self.candidate = Candidate("a", 0)
        self.candidate2 = Candidate("a", 0)
        self.candidate3 = Candidate("a", 1)
        self.candidate4 = Candidate("b", 0)
        self.candidate5 = Candidate("b", 3)

    def test_basic(self):
        self.assertEqual(self.candidate.get_word(), "a")
        self.assertEqual(self.candidate.get_confidence(), 0)

    def test_basic_equal(self):
        self.assertTrue(self.candidate.equals(self.candidate2))
        self.assertFalse(self.candidate.equals(self.candidate3))
        self.assertFalse(self.candidate.equals(self.candidate4))

    def test_basic_compare(self):
        self.assertEqual(self.candidate.compare(self.candidate2), 0)
        self.assertLess(self.candidate.compare(self.candidate3), 0)
        self.assertGreater(self.candidate5.compare(self.candidate4), 0)


if __name__ == '__main__':
    unittest.main()
