from autocompleteprovider import AutoCompleteProvider

def test():
    auto = AutoCompleteProvider()
    auto.insert("'th'")
    auto.insert("'the'")
    auto.insert("'th'")
    print(auto.words)
    print(auto.get_words("'th'"))
    print(auto.words)

if __name__ == "__main__":
    test()